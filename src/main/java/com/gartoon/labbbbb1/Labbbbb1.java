package com.gartoon.labbbbb1;

import java.util.Scanner;

public class Labbbbb1 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int col, row;
    static char player = 'X';
    static int draw = 0;
    static char restart = ' ';

    public static void runMain() {
        while (true) {
            chart();
            input();
            if (checkTable() || checkXY()) {
                chart();
                System.out.println(player + " WIN!");
                System.out.println("You want to Restart Game? Y/N");
                inputRegame();
                if (regame()) {
                    break;
                }
            } else if (checkDraw()) {
                System.out.println("DRAW!");
                System.out.println("You want to Restart Game? Y/N");
                inputRegame();
                if (regame()) {
                    break;
                }
            }
            draw = draw + 1;
            changePlayer();
        }

    }

    public static void input() {
        Scanner ip = new Scanner(System.in);
        int row = ip.nextInt();
        int col = ip.nextInt();
        table[row][col] = player;
    }

    public static void chart() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static void changePlayer() {
        if (player == 'X') {
            System.out.println("TRUN Player O");
            player = 'O';
        } else if (player == 'O') {
            System.out.println("TRUN Player X");
            player = 'X';
        }
    }

    public static boolean checkX() {
        for (int c = 0; c < 3; c++) {
            if (table[0][c] == player && table[1][c] == player && table[2][c] == player) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkY() {
        for (int r = 0; r < 3; r++) {
            if (table[r][0] == player && table[r][1] == player && table[r][2] == player) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkTable() {
        if (checkX()) {
            return true;
        }
        if (checkY()) {
            return true;
        }

        return false;
    }

    public static boolean checkXY() {
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            return true;
        } else if (table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkDraw() {
        if (draw == 8) {
            return true;
        }

        return false;
    }

    public static void inputRegame() {
        Scanner yn = new Scanner(System.in);
        String restart = yn.next();
    }

    public static boolean regame() {
        if (restart.equal("Y")) {
            return false;
        } else {
            return true;
        }
    }
    
    public static void welcomeXO() {
        System.out.println("Welcome to XO GAME");
        System.out.println("TRUN Player X");
    }
    
    public static void main(String[] args) {
        welcomeXO();
        runMain();
    }
    
}


